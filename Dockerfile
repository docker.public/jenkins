FROM jenkins/jenkins:lts

MAINTAINER Miguel Neyra <mneyra@americatel.com.pe>

USER root

RUN apt-get update
RUN apt-get -y install openssh-server && apt-get -y install sshpass && apt-get -y install vim
# libs docker-ce for debian
RUN apt-get -y install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common

RUN curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -

RUN add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
   $(lsb_release -cs) \
   stable"

RUN apt-get update
RUN apt-get -y install docker-ce

RUN mkdir /var/log/jenkins
RUN chown -R jenkins:jenkins /var/log/jenkins
RUN echo "jenkins   ALL=(ALL)   NOPASSWD:ALL" >> /etc/sudoers
RUN chmod -w /etc/sudoers

ENV JAVA_OPTS="-Xmx8192m"
ENV WEB_CONTEXT_ROOT="jenkins"
ENV JENKINS_OPTS="--logfile=/var/log/jenkins/jenkins.log --prefix=/$WEB_CONTEXT_ROOT"

COPY executors.groovy /usr/share/jenkins/ref/init.groovy.d/executors.groovy

USER jenkins