# Jenkins Docker Image
###### Based on Official Jenkins Docker image (https://github.com/jenkinsci/docker)

## Clone Repository and Build Image

Clone git repository https://gitlab.com/docker.public/jenkins.git

```
git clone https://gitlab.com/docker.public/jenkins.git
```

```
cd jenkins/
docker build -t americatelperu/jenkins:lts .
```

## Run Jenkins
```
docker-compose up -d
```